module temperature_test
    use double_precision_generator_m, only: DOUBLE_PRECISION_GENERATOR
    use erloff, only: error_list_t
    use iso_varying_string, only: operator(//)
    use quaff, only: &
            temperature_t, &
            fallible_temperature_t, &
            temperature_unit_t, &
            operator(.unit.), &
            parse_temperature
    use quaff_asserts_m, only: assert_equals, assert_equals_within_relative
    use quaff_gnuplot_units, only: &
            PROVIDED_TEMPERATURE_UNITS, KELVIN
    use quaff_utilities_m, only: PARSE_ERROR
    use temperature_utilities_m, only: &
            units_input_t, units_pair_input_t, make_units_examples
    use units_examples_m, only: units_examples_t
    use vegetables, only: &
            double_precision_input_t, &
            example_t, &
            input_t, &
            result_t, &
            test_item_t, &
            test_result_item_t, &
            assert_equals, &
            assert_equals_within_relative, &
            assert_not, &
            assert_that, &
            describe, &
            fail, &
            it

    implicit none
    private
    public :: test_temperature
contains
    function test_temperature() result(tests)
        type(test_item_t) :: tests

        type(units_examples_t) :: examples

        examples = make_units_examples(PROVIDED_TEMPERATURE_UNITS)
        tests = describe( &
                "temperature_t", &
                [ it( &
                        "returns the same value given the same units", &
                        examples%units(), &
                        check_round_trip) &
                , it( &
                        "preserves its value converting to and from a string", &
                        examples%units(), &
                        check_to_and_from_string) &
                , it( &
                        "returns an error trying to parse a bad string", &
                        check_bad_string) &
                , it( &
                        "returns an error trying to parse an unknown unit", &
                        check_bad_unit) &
                , it( &
                        "returns an error trying to parse a bad number", &
                        check_bad_number) &
                ])
    end function

    function check_round_trip(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        select type (input)
        type is (units_input_t)
            result_ = check_round_trip_in(input%unit())
        class default
            result_ = fail("Expected to get a units_input_t")
        end select
    end function

    function check_round_trip_in(units) result(result_)
        class(temperature_unit_t), intent(in) :: units
        type(result_t) :: result_

        type(test_item_t) :: the_test
        type(test_result_item_t) :: the_result

        the_test = it(units%to_string(), DOUBLE_PRECISION_GENERATOR, check_round_trip_)
        the_result = the_test%run()
        result_ = assert_that(the_result%passed(), the_result%verbose_description(.false.))
    contains
        pure function check_round_trip_(input) result(result__)
            class(input_t), intent(in) :: input
            type(result_t) :: result__

            type(temperature_t) :: intermediate

            select type (input)
            type is (double_precision_input_t)
                intermediate = input%input().unit.units
                result__ = assert_equals_within_relative( &
                        input%input(), &
                        intermediate.in.units, &
                        1.0d-12)
            class default
                result__ = fail("Expected to get a double_precision_input_t")
            end select
        end function
    end function

    function check_to_and_from_string(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        select type (input)
        type is (units_input_t)
            result_ = check_string_trip(input%unit())
        class default
            result_ = fail("Expected to get an units_input_t")
        end select
    end function

    function check_string_trip(units) result(result_)
        class(temperature_unit_t), intent(in) :: units
        type(result_t) :: result_

        type(test_item_t) :: the_test
        type(test_result_item_t) :: the_result

        the_test = it(units%to_string(), DOUBLE_PRECISION_GENERATOR, do_check)
        the_result = the_test%run()
        result_ = assert_that(the_result%passed(), the_result%verbose_description(.false.))
    contains
        function do_check(input) result(result__)
            class(input_t), intent(in) :: input
            type(result_t) :: result__

            type(error_list_t) :: errors
            type(temperature_t) :: original_temperature
            type(fallible_temperature_t) :: maybe_temperature
            type(temperature_t) :: new_temperature

            select type (input)
            type is (double_precision_input_t)
                original_temperature = input%input().unit.units
                maybe_temperature = parse_temperature( &
                        original_temperature%to_string_in(units), PROVIDED_TEMPERATURE_UNITS)
                new_temperature = maybe_temperature%temperature()
                errors = maybe_temperature%errors()
                result__ = &
                        assert_equals( &
                                original_temperature, &
                                new_temperature) &
                        .and.assert_not(errors%has_any(), errors%to_string())
            class default
                result__ = fail("Expected to get a double_precision_input_t")
            end select
        end function
    end function

    function check_bad_string() result(result_)
        type(result_t) :: result_

        type(error_list_t) :: errors
        type(fallible_temperature_t) :: maybe_temperature

        maybe_temperature = parse_temperature("bad", PROVIDED_TEMPERATURE_UNITS)
        errors = maybe_temperature%errors()
        result_ = assert_that(errors.hasType.PARSE_ERROR, errors%to_string())
    end function

    function check_bad_unit() result(result_)
        type(result_t) :: result_

        type(error_list_t) :: errors
        type(fallible_temperature_t) :: maybe_temperature

        maybe_temperature = parse_temperature("1.0 bad", [KELVIN])
        errors = maybe_temperature%errors()
        result_ = assert_that(errors.hasType.PARSE_ERROR, errors%to_string())
    end function

    function check_bad_number() result(result_)
        type(result_t) :: result_

        type(error_list_t) :: errors
        type(fallible_temperature_t) :: maybe_temperature

        maybe_temperature = parse_temperature("bad K", PROVIDED_TEMPERATURE_UNITS)
        errors = maybe_temperature%errors()
        result_ = assert_that(errors.hasType.PARSE_ERROR, errors%to_string())
    end function
end module
