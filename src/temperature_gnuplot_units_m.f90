module temperature_gnuplot_units_m
    use erloff, only: error_list_t, fatal_t, module_t, procedure_t
    use iso_varying_string, only: varying_string, assignment(=), operator(//)
    use parff, only: &
            parse_result_t, &
            parsed_rational_t, &
            parser_output_t, &
            state_t, &
            parse_rational, &
            parse_string, &
            parse_with, &
            then_drop
    use quaff_conversion_factors_m, only: &
            CELSIUS_KELVIN_DIFFERENCE, &
            FAHRENHEIT_RANKINE_DIFFERENCE, &
            RANKINE_PER_KELVIN
    use quaff_utilities_m, only: parse_space, PARSE_ERROR
    use temperature_m, only: &
            temperature_unit_t, &
            fallible_temperature_t, &
            operator(.unit.)

    implicit none
    private
    public :: &
            temperature_gnuplot_unit_t, &
            PROVIDED_UNITS, &
            CELSIUS, &
            FAHRENHEIT, &
            KELVIN, &
            RANKINE

    type, extends(temperature_unit_t), public :: temperature_gnuplot_unit_t
        character(len=100) :: symbol
    contains
        procedure :: unit_to_string
        procedure :: value_to_string
        procedure :: parse_as
    end type

    type(temperature_gnuplot_unit_t), parameter :: CELSIUS = &
            temperature_gnuplot_unit_t( &
                    conversion_factor = 1.0d0, &
                    difference = CELSIUS_KELVIN_DIFFERENCE, &
                    symbol = "{/Symbol \260}C")
    type(temperature_gnuplot_unit_t), parameter :: FAHRENHEIT = &
            temperature_gnuplot_unit_t( &
                    conversion_factor = RANKINE_PER_KELVIN, &
                    difference = FAHRENHEIT_RANKINE_DIFFERENCE, &
                    symbol = "{/Symbol \260}F")
    type(temperature_gnuplot_unit_t), parameter :: KELVIN = &
            temperature_gnuplot_unit_t( &
                    conversion_factor = 1.0d0, &
                    difference = 0.0d0, &
                    symbol = "K")
    type(temperature_gnuplot_unit_t), parameter :: RANKINE = &
            temperature_gnuplot_unit_t( &
                    conversion_factor = RANKINE_PER_KELVIN, &
                    difference = 0.0d0, &
                    symbol = "{/Symbol \260}R")

    type(temperature_gnuplot_unit_t), parameter :: PROVIDED_UNITS(*) = &
            [ CELSIUS &
            , FAHRENHEIT &
            , KELVIN &
            , RANKINE &
            ]
contains
    elemental function unit_to_string(self) result(string)
        class(temperature_gnuplot_unit_t), intent(in) :: self
        type(varying_string) :: string

        string = trim(self%symbol)
    end function

    pure function value_to_string(self, value_) result(string)
        class(temperature_gnuplot_unit_t), intent(in) :: self
        type(varying_string), intent(in) :: value_
        type(varying_string) :: string

        string = value_ // " " // self%to_string()
    end function

    function parse_as(self, string) result(fallible_temperature)
        class(temperature_gnuplot_unit_t), intent(in) :: self
        type(varying_string), intent(in) :: string
        type(fallible_temperature_t) :: fallible_temperature

        type(parse_result_t) :: parse_result

        parse_result = parse_with(the_parser, string)
        if (parse_result%ok()) then
            select type (the_number => parse_result%parsed())
            type is (parsed_rational_t)
                fallible_temperature = fallible_temperature_t(the_number%value_().unit.self)
            end select
        else
            fallible_temperature = fallible_temperature_t(error_list_t(fatal_t( &
                    PARSE_ERROR, &
                    module_t("temperature_gnuplot_units_m"), &
                    procedure_t("parse_as"), &
                    parse_result%message())))
        end if
    contains
        function the_parser(state_) result(result_)
            type(state_t), intent(in) :: state_
            type(parser_output_t) :: result_

            result_ = then_drop( &
                    then_drop(parse_rational, parse_space, state_), &
                    parse_unit)
        end function

        function parse_unit(state_) result(result_)
            type(state_t), intent(in) :: state_
            type(parser_output_t) :: result_

            result_ = parse_string(trim(self%symbol), state_)
        end function
    end function
end module
