module pressure_gnuplot_units_m
    use erloff, only: error_list_t, fatal_t, module_t, procedure_t
    use iso_varying_string, only: varying_string, assignment(=), operator(//)
    use parff, only: &
            parse_result_t, &
            parsed_rational_t, &
            parser_output_t, &
            state_t, &
            parse_rational, &
            parse_string, &
            parse_with, &
            then_drop
    use quaff_conversion_factors_m, only: &
            BAR_PER_PASCAL, &
            DYNES_PER_SQUARE_CENTIMETER_PER_PASCAL, &
            KILOPASCALS_PER_PASCAL, &
            KILOPONDS_PER_SQUARE_CENTIMETER_PER_PASCAL, &
            MEGAPASCALS_PER_PASCAL, &
            POUNDS_PER_SQUARE_INCH_PER_PASCAL
    use quaff_utilities_m, only: parse_space, PARSE_ERROR
    use pressure_m, only: &
            pressure_unit_t, &
            fallible_pressure_t, &
            operator(.unit.)

    implicit none
    private
    public :: &
            pressure_gnuplot_unit_t, &
            PROVIDED_UNITS, &
            BAR, &
            DYNES_PER_SQUARE_CENTIMETER, &
            KILOPASCALS, &
            KILOPONDS_PER_SQUARE_CENTIMETER, &
            MEGAPASCALS, &
            PASCALS, &
            POUNDS_PER_SQUARE_INCH

    type, extends(pressure_unit_t), public :: pressure_gnuplot_unit_t
        character(len=100) :: symbol
    contains
        procedure :: unit_to_string
        procedure :: value_to_string
        procedure :: parse_as
    end type

    type(pressure_gnuplot_unit_t), parameter :: BAR = &
            pressure_gnuplot_unit_t( &
                    conversion_factor = BAR_PER_PASCAL, &
                    symbol = "bar")
    type(pressure_gnuplot_unit_t), parameter :: DYNES_PER_SQUARE_CENTIMETER = &
            pressure_gnuplot_unit_t( &
                    conversion_factor = DYNES_PER_SQUARE_CENTIMETER_PER_PASCAL, &
                    symbol = "dyn/cm^2")
    type(pressure_gnuplot_unit_t), parameter :: KILOPASCALS = &
            pressure_gnuplot_unit_t( &
                    conversion_factor = KILOPASCALS_PER_PASCAL, &
                    symbol = "kPa")
    type(pressure_gnuplot_unit_t), parameter :: KILOPONDS_PER_SQUARE_CENTIMETER = &
            pressure_gnuplot_unit_t( &
                    conversion_factor = KILOPONDS_PER_SQUARE_CENTIMETER_PER_PASCAL, &
                    symbol = "kp/cm^2")
    type(pressure_gnuplot_unit_t), parameter :: MEGAPASCALS = &
            pressure_gnuplot_unit_t( &
                    conversion_factor = MEGAPASCALS_PER_PASCAL, &
                    symbol = "MPa")
    type(pressure_gnuplot_unit_t), parameter :: PASCALS = &
            pressure_gnuplot_unit_t( &
                    conversion_factor = 1.0d0, &
                    symbol = "Pa")
    type(pressure_gnuplot_unit_t), parameter :: POUNDS_PER_SQUARE_INCH = &
            pressure_gnuplot_unit_t( &
                    conversion_factor = POUNDS_PER_SQUARE_INCH_PER_PASCAL, &
                    symbol = "psi")

    type(pressure_gnuplot_unit_t), parameter :: PROVIDED_UNITS(*) = &
            [ BAR &
            , DYNES_PER_SQUARE_CENTIMETER &
            , KILOPASCALS &
            , KILOPONDS_PER_SQUARE_CENTIMETER &
            , MEGAPASCALS &
            , PASCALS &
            , POUNDS_PER_SQUARE_INCH &
            ]
contains
    elemental function unit_to_string(self) result(string)
        class(pressure_gnuplot_unit_t), intent(in) :: self
        type(varying_string) :: string

        string = trim(self%symbol)
    end function

    pure function value_to_string(self, value_) result(string)
        class(pressure_gnuplot_unit_t), intent(in) :: self
        type(varying_string), intent(in) :: value_
        type(varying_string) :: string

        string = value_ // " " // self%to_string()
    end function

    function parse_as(self, string) result(fallible_pressure)
        class(pressure_gnuplot_unit_t), intent(in) :: self
        type(varying_string), intent(in) :: string
        type(fallible_pressure_t) :: fallible_pressure

        type(parse_result_t) :: parse_result

        parse_result = parse_with(the_parser, string)
        if (parse_result%ok()) then
            select type (the_number => parse_result%parsed())
            type is (parsed_rational_t)
                fallible_pressure = fallible_pressure_t(the_number%value_().unit.self)
            end select
        else
            fallible_pressure = fallible_pressure_t(error_list_t(fatal_t( &
                    PARSE_ERROR, &
                    module_t("pressure_gnuplot_units_m"), &
                    procedure_t("parse_as"), &
                    parse_result%message())))
        end if
    contains
        function the_parser(state_) result(result_)
            type(state_t), intent(in) :: state_
            type(parser_output_t) :: result_

            result_ = then_drop( &
                    then_drop(parse_rational, parse_space, state_), &
                    parse_unit)
        end function

        function parse_unit(state_) result(result_)
            type(state_t), intent(in) :: state_
            type(parser_output_t) :: result_

            result_ = parse_string(trim(self%symbol), state_)
        end function
    end function
end module
