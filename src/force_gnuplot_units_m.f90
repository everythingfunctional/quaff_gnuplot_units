module force_gnuplot_units_m
    use erloff, only: error_list_t, fatal_t, module_t, procedure_t
    use iso_varying_string, only: varying_string, assignment(=), operator(//)
    use parff, only: &
            parse_result_t, &
            parsed_rational_t, &
            parser_output_t, &
            state_t, &
            parse_rational, &
            parse_string, &
            parse_with, &
            then_drop
    use quaff_conversion_factors_m, only: &
            DYNES_PER_NEWTON, &
            KILOPONDS_PER_NEWTON, &
            MILLINEWTONS_PER_NEWTON, &
            POUNDS_PER_NEWTON
    use quaff_utilities_m, only: parse_space, PARSE_ERROR
    use force_m, only: &
            force_unit_t, &
            fallible_force_t, &
            operator(.unit.)

    implicit none
    private
    public :: &
            force_gnuplot_unit_t, &
            PROVIDED_UNITS, &
            DYNES, &
            KILOPONDS, &
            MILLINEWTONS, &
            NEWTONS, &
            POUNDS

    type, extends(force_unit_t), public :: force_gnuplot_unit_t
        character(len=100) :: symbol
    contains
        procedure :: unit_to_string
        procedure :: value_to_string
        procedure :: parse_as
    end type

    type(force_gnuplot_unit_t), parameter :: DYNES = &
            force_gnuplot_unit_t( &
                    conversion_factor = DYNES_PER_NEWTON, &
                    symbol = "dyn")
    type(force_gnuplot_unit_t), parameter :: KILOPONDS = &
            force_gnuplot_unit_t( &
                    conversion_factor = KILOPONDS_PER_NEWTON, &
                    symbol = "kp")
    type(force_gnuplot_unit_t), parameter :: MILLINEWTONS = &
            force_gnuplot_unit_t( &
                    conversion_factor = MILLINEWTONS_PER_NEWTON, &
                    symbol = "mN")
    type(force_gnuplot_unit_t), parameter :: NEWTONS = &
            force_gnuplot_unit_t( &
                    conversion_factor = 1.0d0, &
                    symbol = "N")
    type(force_gnuplot_unit_t), parameter :: POUNDS = &
            force_gnuplot_unit_t( &
                    conversion_factor = POUNDS_PER_NEWTON, &
                    symbol = "lbf")

    type(force_gnuplot_unit_t), parameter :: PROVIDED_UNITS(*) = &
            [ DYNES &
            , KILOPONDS &
            , MILLINEWTONS &
            , NEWTONS &
            , POUNDS &
            ]
contains
    elemental function unit_to_string(self) result(string)
        class(force_gnuplot_unit_t), intent(in) :: self
        type(varying_string) :: string

        string = trim(self%symbol)
    end function

    pure function value_to_string(self, value_) result(string)
        class(force_gnuplot_unit_t), intent(in) :: self
        type(varying_string), intent(in) :: value_
        type(varying_string) :: string

        string = value_ // " " // self%to_string()
    end function

    function parse_as(self, string) result(fallible_force)
        class(force_gnuplot_unit_t), intent(in) :: self
        type(varying_string), intent(in) :: string
        type(fallible_force_t) :: fallible_force

        type(parse_result_t) :: parse_result

        parse_result = parse_with(the_parser, string)
        if (parse_result%ok()) then
            select type (the_number => parse_result%parsed())
            type is (parsed_rational_t)
                fallible_force = fallible_force_t(the_number%value_().unit.self)
            end select
        else
            fallible_force = fallible_force_t(error_list_t(fatal_t( &
                    PARSE_ERROR, &
                    module_t("force_gnuplot_units_m"), &
                    procedure_t("parse_as"), &
                    parse_result%message())))
        end if
    contains
        function the_parser(state_) result(result_)
            type(state_t), intent(in) :: state_
            type(parser_output_t) :: result_

            result_ = then_drop( &
                    then_drop(parse_rational, parse_space, state_), &
                    parse_unit)
        end function

        function parse_unit(state_) result(result_)
            type(state_t), intent(in) :: state_
            type(parser_output_t) :: result_

            result_ = parse_string(trim(self%symbol), state_)
        end function
    end function
end module
