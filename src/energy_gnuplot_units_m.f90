module energy_gnuplot_units_m
    use erloff, only: error_list_t, fatal_t, module_t, procedure_t
    use iso_varying_string, only: varying_string, assignment(=), operator(//)
    use parff, only: &
            parse_result_t, &
            parsed_rational_t, &
            parser_output_t, &
            state_t, &
            parse_rational, &
            parse_string, &
            parse_with, &
            then_drop
    use quaff_conversion_factors_m, only: &
            BTU_PER_JOULE, &
            CALORIES_PER_JOULE, &
            KILOJOULES_PER_JOULE, &
            MEGABTU_PER_JOULE, &
            MEGAWATT_DAYS_PER_JOULE
    use quaff_utilities_m, only: parse_space, PARSE_ERROR
    use energy_m, only: &
            energy_unit_t, &
            fallible_energy_t, &
            operator(.unit.)

    implicit none
    private
    public :: &
            energy_gnuplot_unit_t, &
            PROVIDED_UNITS, &
            BTU, &
            CALORIES, &
            JOULES, &
            KILOJOULES, &
            MEGABTU, &
            MEGAWATT_DAYS

    type, extends(energy_unit_t), public :: energy_gnuplot_unit_t
        character(len=100) :: symbol
    contains
        procedure :: unit_to_string
        procedure :: value_to_string
        procedure :: parse_as
    end type

    type(energy_gnuplot_unit_t), parameter :: BTU = &
            energy_gnuplot_unit_t( &
                    conversion_factor = BTU_PER_JOULE, &
                    symbol = "BTU")
    type(energy_gnuplot_unit_t), parameter :: CALORIES = &
            energy_gnuplot_unit_t( &
                    conversion_factor = CALORIES_PER_JOULE, &
                    symbol = "cal")
    type(energy_gnuplot_unit_t), parameter :: JOULES = &
            energy_gnuplot_unit_t( &
                    conversion_factor = 1.0d0, &
                    symbol = "J")
    type(energy_gnuplot_unit_t), parameter :: KILOJOULES = &
            energy_gnuplot_unit_t( &
                    conversion_factor = KILOJOULES_PER_JOULE, &
                    symbol = "kJ")
    type(energy_gnuplot_unit_t), parameter :: MEGABTU = &
            energy_gnuplot_unit_t( &
                    conversion_factor = MEGABTU_PER_JOULE, &
                    symbol = "MBTU")
    type(energy_gnuplot_unit_t), parameter :: MEGAWATT_DAYS = &
            energy_gnuplot_unit_t( &
                    conversion_factor = MEGAWATT_DAYS_PER_JOULE, &
                    symbol = "MW d")

    type(energy_gnuplot_unit_t), parameter :: PROVIDED_UNITS(*) = &
            [ BTU &
            , CALORIES &
            , JOULES &
            , KILOJOULES &
            , MEGABTU &
            , MEGAWATT_DAYS &
            ]
contains
    elemental function unit_to_string(self) result(string)
        class(energy_gnuplot_unit_t), intent(in) :: self
        type(varying_string) :: string

        string = trim(self%symbol)
    end function

    pure function value_to_string(self, value_) result(string)
        class(energy_gnuplot_unit_t), intent(in) :: self
        type(varying_string), intent(in) :: value_
        type(varying_string) :: string

        string = value_ // " " // self%to_string()
    end function

    function parse_as(self, string) result(fallible_energy)
        class(energy_gnuplot_unit_t), intent(in) :: self
        type(varying_string), intent(in) :: string
        type(fallible_energy_t) :: fallible_energy

        type(parse_result_t) :: parse_result

        parse_result = parse_with(the_parser, string)
        if (parse_result%ok()) then
            select type (the_number => parse_result%parsed())
            type is (parsed_rational_t)
                fallible_energy = fallible_energy_t(the_number%value_().unit.self)
            end select
        else
            fallible_energy = fallible_energy_t(error_list_t(fatal_t( &
                    PARSE_ERROR, &
                    module_t("energy_gnuplot_units_m"), &
                    procedure_t("parse_as"), &
                    parse_result%message())))
        end if
    contains
        function the_parser(state_) result(result_)
            type(state_t), intent(in) :: state_
            type(parser_output_t) :: result_

            result_ = then_drop( &
                    then_drop(parse_rational, parse_space, state_), &
                    parse_unit)
        end function

        function parse_unit(state_) result(result_)
            type(state_t), intent(in) :: state_
            type(parser_output_t) :: result_

            result_ = parse_string(trim(self%symbol), state_)
        end function
    end function
end module
