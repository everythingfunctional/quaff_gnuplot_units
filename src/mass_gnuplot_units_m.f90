module mass_gnuplot_units_m
    use erloff, only: error_list_t, fatal_t, module_t, procedure_t
    use iso_varying_string, only: varying_string, assignment(=), operator(//)
    use parff, only: &
            parse_result_t, &
            parsed_rational_t, &
            parser_output_t, &
            state_t, &
            parse_rational, &
            parse_string, &
            parse_with, &
            then_drop
    use quaff_conversion_factors_m, only: &
            GRAMS_PER_KILOGRAM, POUNDS_PER_KILOGRAM, TONS_PER_KILOGRAM
    use quaff_utilities_m, only: parse_space, PARSE_ERROR
    use mass_m, only: &
            mass_unit_t, &
            fallible_mass_t, &
            operator(.unit.)

    implicit none
    private
    public :: &
            mass_gnuplot_unit_t, &
            PROVIDED_UNITS, &
            GRAMS, &
            KILOGRAMS, &
            POUNDS_MASS, &
            TONS

    type, extends(mass_unit_t), public :: mass_gnuplot_unit_t
        character(len=100) :: symbol
    contains
        procedure :: unit_to_string
        procedure :: value_to_string
        procedure :: parse_as
    end type

    type(mass_gnuplot_unit_t), parameter :: GRAMS = &
            mass_gnuplot_unit_t( &
                    conversion_factor = GRAMS_PER_KILOGRAM, &
                    symbol = "g")
    type(mass_gnuplot_unit_t), parameter :: KILOGRAMS = &
            mass_gnuplot_unit_t( &
                    conversion_factor = 1.0d0, &
                    symbol = "kg")
    type(mass_gnuplot_unit_t), parameter :: POUNDS_MASS = &
            mass_gnuplot_unit_t( &
                    conversion_factor = POUNDS_PER_KILOGRAM, &
                    symbol = "lbm")
    type(mass_gnuplot_unit_t), parameter :: TONS = &
            mass_gnuplot_unit_t( &
                    conversion_factor = TONS_PER_KILOGRAM, &
                    symbol = "t")

    type(mass_gnuplot_unit_t), parameter :: PROVIDED_UNITS(*) = &
            [ GRAMS &
            , KILOGRAMS &
            , POUNDS_MASS &
            , TONS &
            ]
contains
    elemental function unit_to_string(self) result(string)
        class(mass_gnuplot_unit_t), intent(in) :: self
        type(varying_string) :: string

        string = trim(self%symbol)
    end function

    pure function value_to_string(self, value_) result(string)
        class(mass_gnuplot_unit_t), intent(in) :: self
        type(varying_string), intent(in) :: value_
        type(varying_string) :: string

        string = value_ // " " // self%to_string()
    end function

    function parse_as(self, string) result(fallible_mass)
        class(mass_gnuplot_unit_t), intent(in) :: self
        type(varying_string), intent(in) :: string
        type(fallible_mass_t) :: fallible_mass

        type(parse_result_t) :: parse_result

        parse_result = parse_with(the_parser, string)
        if (parse_result%ok()) then
            select type (the_number => parse_result%parsed())
            type is (parsed_rational_t)
                fallible_mass = fallible_mass_t(the_number%value_().unit.self)
            end select
        else
            fallible_mass = fallible_mass_t(error_list_t(fatal_t( &
                    PARSE_ERROR, &
                    module_t("mass_gnuplot_units_m"), &
                    procedure_t("parse_as"), &
                    parse_result%message())))
        end if
    contains
        function the_parser(state_) result(result_)
            type(state_t), intent(in) :: state_
            type(parser_output_t) :: result_

            result_ = then_drop( &
                    then_drop(parse_rational, parse_space, state_), &
                    parse_unit)
        end function

        function parse_unit(state_) result(result_)
            type(state_t), intent(in) :: state_
            type(parser_output_t) :: result_

            result_ = parse_string(trim(self%symbol), state_)
        end function
    end function
end module
